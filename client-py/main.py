#!/usr/bin/env python3

from pottery import  Redlock, RedisDict
from redis import Redis
import time
import random as rn
import sys

key = 'gilda'
max_jobs = 10

def print_title(title):
    print(f"\n\n{title}\n")
    print('='*80)
    print("\n")

def print_message(process, message):
    print(f"    [{process}] {message}")

def run_test(process, add=True):
    print_title(f"Process {process} - add process={add}")
    # add process will add random jobs when possible in a loop to simulate queue inputs
    # not add process will remove jobs in a rate of 10%
    r=[None]*50
    rl=[None]*50
    if add:
        init_delay = rn.randint(1,5)
        max_cycles = rn.randint(2,5)
        print_message(process, f"waiting {init_delay} secs to start {max_cycles} cycle/s")
        time.sleep(init_delay)
    else:
        max_cycles = 50

    print_message(process, 'starting')
    print_message(process, 'creating redis...')
    r[process] = Redis(host= "redis", port= 6379)
    print_message(process, 'creating redlock with 20sec ttl...')
    rl[process] = Redlock(masters={r[process]},key=key, auto_release_time=20000, context_manager_timeout=1000)
    print_message(process, f"key {rl[process].key} is locked? {rl[process].locked()}")

    cycle = 0
    while cycle < max_cycles:
        print_message(process, f"cycle {cycle}/{max_cycles}...")
        cycle += 1

        if add:
            # here we should be reading the queue looking for jobs
            job2sched = rn.randint(1,10)
            print_message(process, f"using key {key} -- jobs to schedule {job2sched}")
        else:
            job2sched = 1

        print_message(process, 'getting lock...')
        try:
            remaining_jobs = job2sched
            while remaining_jobs > 0:
            # the number of jobs will be added to redis and then (if success) we will be ready and ok_to_go
                ok_to_go = False
                max_tries = 5
                try_number = 0
                try_waiting_time = 10
                while not ok_to_go and try_number < max_tries:
                    try_number += 1
                    print_message(process, f"try {try_number}/{max_tries}...")
                    if try_number > 1:
                        time.sleep(try_waiting_time)
                        with rl[process]:
                            print_message(process, f"lock acquired, key is locked? {rl[process].locked()}")
                            value = r[process].get(key)
                            if value is None:
                                value = 0
                            if add:
                                print_message(process, f"checking whether {max_jobs} - {value} is greater than {job2sched}")
                                if (max_jobs - int(value)) >= job2sched:
                                    # all jobs can bu submitted
                                    print_message(process, f"prev value {value} -- new value {int(value)+job2sched} ---------------------------")
                                    r[process].set(key,int(value)+job2sched)
                                    final_delay = rn.randint(1,2)
                                    print_message(process, f"waiting {final_delay} secs after set keeping the lock")
                                    time.sleep(final_delay)
                                    ok_to_go = True
                                    remaining_jobs = 0
                                elif (max_jobs - int(value)) > 0:
                                    # at least one job can bu submitted
                                    remaining_jobs = job2sched - (max_jobs - int(value))
                                    job2sched = (max_jobs - int(value))
                                    print_message(process, f"prev value {value} -- ne value {int(value)+job2sched} PARTIAL --------------------")
                                    r[process].set(key,int(value)+job2sched)
                                    final_delay = rn.randint(1,2)
                                    print_message(process, f"waiting {final_delay} secs after set keeping the lock")
                                    time.sleep(final_delay)
                                    ok_to_go = True
                                else:
                                   print_message(process, f"condition not met, waiting antoher cycle")
                            else:
                                finished_perc = round(rn.random(),2)
                                print_message(process, f"finishing {finished_perc}%")
                                finished_jobs = round((int(value) * finished_perc) + 0.5)
                                print_message(process, f"prev value {value} -- new value {int(value)-finished_jobs} ---------------------------")
                                r[process].set(key,int(value)-finished_jobs)
                                ok_to_go = True
                if not ok_to_go:
                    raise Exception('max tries reached!')
                else:
                    if add:
                        print_message(process, f"submitting {job2sched} jobs to Textract...")
                        job2sched = remaining_jobs
                    else:
                        remaining_jobs = 0
        except Exception as e:
            raise e
        print_message(process, f"released, key is locked? {rl[process].locked()}")
        final_delay = rn.randint(5,10)
        print_message(process, f"waiting {final_delay} secs with no lock")
        time.sleep(final_delay)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        process = sys.argv[1]
    else:
        process = rn.randint(0,100)
    if process != "-1":
        run_test(int(process))
    else:
        with Redis(host= "redis", port= 6379) as r:
            r.delete(key)

        run_test(int(process), add=False)

        print_title('Final result')
        r = Redis(host= "redis", port= 6379)
        rl = Redlock(masters={r},key=key, auto_release_time=20000, context_manager_timeout=1000)
        print_message(process, 'getting lock...')
        with rl:
            value = r.get(key)
            print_message(process, f"Final value is {value}")

    print_message(process, "THE END")
